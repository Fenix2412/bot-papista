extern crate yaml_rust;

use yaml_rust::{YamlLoader};
use teloxide::{prelude::*, utils::command::BotCommand, types::*, requests::Request, requests::RequestWithFile};
use std::fs::File;
use std::io::Read;
use std::env;
use rand::prelude::*;
use std::path::*;

#[derive(BotCommand)]
#[command(rename = "lowercase", description = "Następujące komendy są dostępne:")]
enum Command {
    #[command(description = "pomoc.")]
    Pomoc,
    #[command(description = "czy można?")]
    Czymozna(String),
    #[command(description = "ID?")]
    Chatid,
    //#[command(description = "handle a username and an age.", parse_with = "split")]
    //UsernameAndAge { username: String, age: u8 },
}

struct Config {
    token: String,
    groups: Vec<i64>,
    papaj_gif_path: String,
    papaj_audio_path: String
}

async fn answer(cx: UpdateWithCx<Message>, command: Command) -> ResponseResult<()> {
    match command {
        Command::Pomoc => cx.answer(Command::descriptions()).send().await?,
        Command::Czymozna(text) => {
            let random = rand::random::<bool>();
            let string = match random {
                true => "Czy można %placeholder%? A można, jak najbardziej. Jeszcze jak!",
                false => "Czy można %placeholder%? Nie... Nie wiem..."
            };
            cx.answer_str(string.replace("%placeholder%", &text)).await?
        },
        Command::Chatid =>  {
            let chatid = cx.chat_id().to_string();
            cx.answer(chatid).send().await?
        },
        //Command::UsernameAndAge { username, age } => {
        //    cx.answer_str(format!("Your username is @{} and age is {}.", username, age)).await?
        //}
    };

    Ok(())
}

#[tokio::main]
async fn main() {
    let args: Vec<String> = env::args().collect();
    let search = String::from("--2137");
    match args.contains(&search) {
        true => { papaj().await; },
        false => { run().await; }
    };
}

async fn papaj() {
    teloxide::enable_logging!();
    log::info!("Wysyłam papaja");

    let config = load_config("./local_config/papista.yaml");
    let bot = Bot::builder().token(config.token).build();

    let photo_path = PathBuf::from(config.papaj_gif_path);
    let audio_path = PathBuf::from(config.papaj_audio_path);
    

    for group_id in config.groups {
        let chatid = ChatId::Id(group_id);

        let photo_file = InputFile::file(&photo_path);
        let audio_file = InputFile::file(&audio_path);
        bot.send_message(chatid.clone(), "Inba").send().await.unwrap();
        bot.send_document(chatid.clone(), photo_file).send().await.unwrap().unwrap();
        bot.send_audio(chatid.clone(), audio_file).send().await.unwrap().unwrap();
    }
}

async fn run() {
    teloxide::enable_logging!();
    log::info!("Odpalam papiste...");

    let config = load_config("./local_config/papista.yaml");
    let bot = Bot::builder().token(config.token).build();

    let bot_name = "Papista";
    teloxide::commands_repl(bot, bot_name, answer).await;
}

fn load_config(file_name: &str) -> Config {
    let mut config_file = File::open(file_name).unwrap();
    let mut config_contents = String::new();
    config_file.read_to_string(&mut config_contents).unwrap();
    let config = YamlLoader::load_from_str(&config_contents).unwrap();
    let mut groups: Vec<i64> = Vec::new();

    for group_id in config[0]["groups"].as_vec().unwrap() {
        groups.push(group_id.as_i64().unwrap())
    }

    Config {
        token: String::from(config[0]["token"].as_str().unwrap()),
        groups: groups,
        papaj_gif_path: String::from(config[0]["gif_path"].as_str().unwrap()),
        papaj_audio_path: String::from(config[0]["audio_path"].as_str().unwrap()),
    }
}